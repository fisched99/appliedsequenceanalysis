import os

rule trinity:
    input:
        r1 = expand(os.path.join(config['output_directory'], "01-QC", "trim", "{sample}.1.fastq"), sample=list(samples.index)),
        r2 = expand(os.path.join(config['output_directory'], "01-QC", "trim", "{sample}.2.fastq"), sample=list(samples.index))
    output:
        outdir    = directory(os.path.join(config['output_directory'], "03-ASSEMBLY", "trinity")),
        contigs   = os.path.join(config['output_directory'], "03-ASSEMBLY", "trinity.Trinity.fasta")
    log:
        os.path.join(config['output_directory'], "00-Log", "assembly", "trinity.log")
    conda:
        os.path.join("..", "envs", "trinity.yaml")
    threads:
        32
    shell:
        """
        Trinity --seqType fq --left {input.r1} --right {input.r2} --CPU {threads} --output {output.outdir} --max_memory 20G &> {log}
        """


rule hisat2_idx:
    input:
        reference = config['reference']
    output:
        index = multiext(os.path.join(config['output_directory'], "03-ASSEMBLY", "hisat2-index", f"{os.path.splitext(os.path.basename(config['reference']))[0]}"), '.1.ht2', '.2.ht2', '.3.ht2', '.4.ht2', '.5.ht2', '.6.ht2', '.7.ht2', '.8.ht2')
    log:
        os.path.join(config['output_directory'], "00-Log", "assembly", "hisat2_idx.log")
    conda:
        os.path.join("..", "envs", "assembly.yaml")
    threads:
        16
    params:
        # magic to build the stupid index thinbg (on the verge of crying splitext() is the reason we cant have nice things)
        idx_prefix = lambda w, output: os.path.splitext(os.path.splitext(output[0])[0])[0]

    shell:
        """
        hisat2-build -p {threads} {input.reference} {params.idx_prefix} &> {log}
        """


rule hisat2_map:
    input:
        r1 = os.path.join(config['output_directory'], "01-QC", "trim", "{sample}.1.fastq"),
        r2 = os.path.join(config['output_directory'], "01-QC", "trim", "{sample}.2.fastq"),
        index = multiext(os.path.join(config['output_directory'], "03-ASSEMBLY", "hisat2-index", f"{os.path.splitext(os.path.basename(config['reference']))[0]}"), '.1.ht2', '.2.ht2', '.3.ht2', '.4.ht2', '.5.ht2', '.6.ht2', '.7.ht2', '.8.ht2')
    output:
        sorted_bam = os.path.join(config['output_directory'], "03-ASSEMBLY", "hisat2_map", "{sample}_sorted.bam")
    log:
        os.path.join(config['output_directory'], "00-Log", "assembly", "{sample}_hisat2_map.log")
    conda:
        os.path.join("..", "envs", "assembly.yaml")
    threads:
        6
    params:
        # to merge input, hisat2 can take comma seperated list of samples but
        # snakesnake seperates input lists by whitespace so we try to convert to comma seperate
        # (I have no idea what I'm doing why would you seperate lists by whiotespace what tthe hell)
        # doesnt work giving up now, we'll just merge the sorted bam files or smth idk
        #r1 = lambda w, input: input[0].replace(' ', ','),
        #r2 = lambda w, input: input[1].replace(' ', ','),
        idx_path = os.path.join(config['output_directory'], "03-ASSEMBLY", "hisat2-index"),
        idx_prefix = os.path.splitext(os.path.basename(config['reference']))[0]
    shell:
        """
        # why the fu can I only specify the index path via env variable omg why is there no flag WHYY
        export HISAT2_INDEXES='{params.idx_path}'
        hisat2 -x {params.idx_prefix} -1 {input.r1} -2 {input.r2} -p {threads} | samtools view -bS | samtools sort -o {output} -T tmp --threads {threads} 2> {log}
        """

rule samtools_merge:
    input:
        sorted_bams = expand(os.path.join(config['output_directory'], "03-ASSEMBLY", "hisat2_map", "{sample}_sorted.bam"), sample = list(samples.index))
    output:
        merged_bam = os.path.join(config['output_directory'], "03-ASSEMBLY", "samtools", "merged.bam")
    log:
        os.path.join(config['output_directory'], "00-Log", "assembly", "samtools_merge.log")
    conda:
        os.path.join("..", "envs", "assembly.yaml")
    threads:
        6
    shell:
        """
        samtools merge {output.merged_bam} {input.sorted_bams} 2> {log}
        """
