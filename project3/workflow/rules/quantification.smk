rule kallisto_idx:
    input:
        contigs   = os.path.join(config['output_directory'], "03-ASSEMBLY", "trinity.Trinity.fasta")
    output:
        index     = os.path.join(config['output_directory'], "04-COUNTS", "kallisto_idx", "kallisto.idx")
    log:
        os.path.join(config['output_directory'], "00-Log", "counts", "kallisto_idx.log")
    conda:
        os.path.join("..", "envs", "quantification.yaml")
    threads:
        2
    shell:
        """
        kallisto index -i {output.index} {input.contigs}
        """

rule kallisto_quant:
    input:
        index    = os.path.join(config['output_directory'], "04-COUNTS", "kallisto_idx", "kallisto.idx"),
        r1       = expand(os.path.join(config['output_directory'], "01-QC", "trim", "{sample}.1.fastq"),sample=list(samples.index)),
        r2       = expand(os.path.join(config['output_directory'], "01-QC", "trim", "{sample}.2.fastq"),sample=list(samples.index))
    output:
        outdir   = directory(os.path.join(config['output_directory'], "04-COUNTS", "kallisto")),
        counts   = os.path.join(config['output_directory'], "04-COUNTS", "kallisto", "abundance.tsv")
    log:
        os.path.join(config['output_directory'], "00-Log", "counts", "kallisto.log")
    conda:
        os.path.join("..", "envs", "quantification.yaml")
    threads:
        2
    shell:
        """
        kallisto quant -i {input.index} -o {output.outdir} {input.r1} {input.r2}
        """

rule featurecounts:
    input:
        bam = os.path.join(config['output_directory'], "03-ASSEMBLY", "samtools", "merged.bam"),
        annotation = config['annotation']
    output:
        counts   = os.path.join(config['output_directory'], "04-COUNTS", "featurecounts", "counts.tsv")
    log:
        os.path.join(config['output_directory'], "00-Log", "counts", "featurecounts.log")
    conda:
        os.path.join("..", "envs", "quantification.yaml")
    threads:
        2
    params:
        featurecounts_params = config['featurecounts']['params']
    shell:
        """
        featureCounts -pBP -T {threads} -a {input.annotation} -o {output.counts} {params.featurecounts_params} {input.bam} 2> {log}
        """