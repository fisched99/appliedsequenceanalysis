import matplotlib.pyplot as plt
from Bio import AlignIO
from Bio import Phylo
import argparse
import numpy as np

parser = argparse.ArgumentParser(description="Description of your program", epilog="Example usage: python3 create_input_csv.py <MSA> <WINDOW_SIZE> <VAR_PLOT>" )
parser.add_argument("MSA", help="File containing the multiple sequence alignment.")
parser.add_argument("WINDOW_SIZE", help="Size of the sliding window in base pairs.")
parser.add_argument("VAR_PLOT", help="Variability plot output file name.")
parser.add_argument("TREE_PLOT", help="Tree plot output file name.")
parser.add_argument("TREE_NWK", help="Tree input file name. (newick formatted)")
args = parser.parse_args()

msa_file       = args.MSA
window_size    = int(args.WINDOW_SIZE)
var_plot       = args.VAR_PLOT
tree_plot      = args.TREE_PLOT
tree_nwk       = args.TREE_NWK

variabilities  = []
means          = []

msa = AlignIO.read(open(msa_file), "fasta")

for i in range(0, msa.get_alignment_length()-1):
    column = [record.seq[i] for record in msa]
    var = 0

    for nuc in ['a', 'c', 'g', 't']:
        ni = column.count(nuc)
        if(ni == 0):
            var = 0
        else:
            var += ((ni/len(column)) * np.log(ni/len(column))) * (-1)
    
    variabilities.append(var)

for i in range(0, len(variabilities)-window_size):
    means.append(np.mean(variabilities[i:i+window_size]))

plt.xlabel(f"{window_size} bp sliding window starting position in bp")
plt.ylabel(f"Mean variability")
plt.title(f"Mean variability over {window_size} bp sliding window")
plt.plot(means)
plt.savefig(var_plot)

Phylo.draw(Phylo.read(tree_nwk, "newick"), do_show = False)
plt.title(f"Phylogenetic tree over all assembled genomes")
plt.savefig(tree_plot)




