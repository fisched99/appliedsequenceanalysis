from genericpath import exists
import pandas as pd

class bcolors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKCYAN = '\033[96m'
    OKGREEN = '\033[92m'
    WARN = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'

BANNER = f"""
{bcolors.HEADER}
   ___                     _ __    _ __                     ___     _ __                          
  / __|     _ _   _  _    | '_ \  | '_ \   ___      o O O  / __|   | '_ \  __ _     ___     ___   
 | (_ |    | '_| | +| |   | .__/  | .__/  / -_)    o       \__ \   | .__/ / _` |   (_-<    (_-<   
  \___|   _|_|_   \_,_|   |_|__   |_|__   \___|   TS__[O]  |___/   |_|__  \__,_|   /__/_   /__/_  
_|\"\"\"\"\"|_|\"\"\"\"\"|_|\"\"\"\"\"|_|\"\"\"\"\"|_|\"\"\"\"\"|_|\"\"\"\"\"| {{======|_|\"\"\"\"\"|_|\"\"\"\"\"|_|\"\"\"\"\"|_|\"\"\"\"\"|_|\"\"\"\"\"| 
"`-0-0-'"`-0-0-'"`-0-0-'"`-0-0-'"`-0-0-'"`-0-0-'./o--000'"`-0-0-'"`-0-0-'"`-0-0-'"`-0-0-'"`-0-0-'  
{bcolors.ENDC}
"""

if not exists(os.path.join(config['output_directory'], ".header.lock")): 
    print(f"{BANNER}")
    os.makedirs(config['output_directory'], exist_ok=True)
    with open(os.path.join(config['output_directory'], ".header.lock"), 'w'): pass

HELPMSG = f"""
{bcolors.OKBLUE}usage: snakemake --config reads=input.csv metadata_csv=meta.csv contrast_csv=contr.csv formula=cond ... [custom parameters] ... --profile profiles/local/ {bcolors.ENDC}
{bcolors.WARN}### Required parameters{bcolors.ENDC}
"""

def path_check(path, param):
    if path:
        if path[-1] == "/":
            path = path[0:-1]
        if not exists(path):
            print(f"{bcolors.FAIL}FAIL: {path}: No such file or directory, exiting.{bcolors.ENDC}")
            exit()
    else:
        print(f"{bcolors.WARN}WARN: No argument provided for flag {param}.{bcolors.ENDC}")


path_check(config["samples"], "samples")


