# AppliedSequenceAnalysis
## Group 2
Project code for the masters course applied sequence analysis.

## Project 1
This project contains the basics of Snakemake and was part of the Snakemake tutorial of the course.

## Project 2
This project contains a workflow to process (viral) NGS data including quality control, trimming, decontamination, denovo assembly, assembly polishing, scaffolding, clustering of the assemblies and per base variance analysis.
