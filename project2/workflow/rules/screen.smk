rule kraken2:
    input:
        r1 = os.path.join(config['output_directory'], "01-QC", "trim", "{sample}.1.fastq"),
        r2 = os.path.join(config['output_directory'], "01-QC", "trim", "{sample}.2.fastq")
    output:
        os.path.join(config['output_directory'], "02-SCREEN", "kraken2", "{sample}_report.txt")
    params:
        kraken2_db = config["kraken2_db"]
    log:
        os.path.join(config['output_directory'], "00-Log", "kraken2", "{sample}_kraken2.log")
    conda:
        os.path.join("..", "envs", "screen.yaml")
    threads:
        16
    shell:
        """
        kraken2 --db {params.kraken2_db} --threads {threads} --paired --report {output} {input.r1} {input.r2} 2> {log} > /dev/null # we dont care about the actual output that is written to stdout (and is quite large)
        """


rule screen:
    input:
        expand(os.path.join(config['output_directory'], "02-SCREEN", "kraken2", "{sample}_report.txt"), sample = list(samples.index))
    output:
        os.path.join(config['output_directory'], "02-SCREEN", "multiqc_screen_report.html")
    params:
        extra="",  # Optional: extra parameters for multiqc.
        use_input_files_only=True, # Optional, use only a.txt and don't search folder samtools_stats for files
    log:
        os.path.join(config['output_directory'], "00-Log", "multiqc", "mutliqc_screen.log")
    wrapper:
        "v1.31.1/bio/multiqc"
