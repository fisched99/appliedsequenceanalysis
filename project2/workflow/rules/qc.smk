
rule fastp:
    input:
        sample = lambda wildcards: [ samples.at[wildcards.sample, 'fq1'], samples.at[wildcards.sample, 'fq2']]
    output:
        trimmed=[os.path.join(config['output_directory'], "01-QC", "trim", "{sample}.1.fastq"),
                 os.path.join(config['output_directory'], "01-QC", "trim", "{sample}.2.fastq")],

        # Unpaired reads separately
        unpaired1=os.path.join(config['output_directory'], "01-QC", "trim", "{sample}.u1.fastq"),
        unpaired2=os.path.join(config['output_directory'], "01-QC", "trim", "{sample}.u2.fastq"),

        #merged="trimmed/pe/{sample}.merged.fastq",
        failed=os.path.join(config['output_directory'], "01-QC", "trim", "{sample}.failed.fastq"),
        html=os.path.join(config['output_directory'], "01-QC", "qc", "{sample}.html"),
        json=os.path.join(config['output_directory'], "01-QC", "qc", "{sample}.json")
    log:
        os.path.join(config['output_directory'], "00-Log", "qc", "{sample}_fastp.log")
    params:
        #adapters="--adapter_sequence ACGGCTAGCTA --adapter_sequence_r2 AGATCGGAAGAGCACACGTCTGAACTCCAGTCAC"
        #extra="--merge"
    threads: 2
    wrapper:
        "v1.31.1/bio/fastp"


rule multiqc:
    input:
        expand(os.path.join(config['output_directory'], "01-QC", "qc", "{sample}.json"), sample = list(samples.index))
    output:
        os.path.join(config['output_directory'], "01-QC", "multiqc.html")
    params:
        extra="",  # Optional: extra parameters for multiqc.
        use_input_files_only=True, # Optional, use only a.txt and don't search folder samtools_stats for files
    log:
        os.path.join(config['output_directory'], "00-Log", "multiqc", "mutliqc_all.log")
    wrapper:
        "v1.31.1/bio/multiqc"
