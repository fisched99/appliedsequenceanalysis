rule mafft:
    input:
        assemblies = expand(os.path.join(config['output_directory'], "03-ASSEMBLY", "ragtag", "{sample}", "main.scaffold.fasta"), sample = list(samples.index))
    output:
        msa    = os.path.join(config['output_directory'], "04-PHYLOGENY", "msa.fasta"),
        all    = temp("all.fasta")
    log:
        os.path.join(config['output_directory'], "00-Log", "phylogeny", "mafft.log")
    threads: 
        8
    conda:
        os.path.join("..", "envs", "phylogeny.yaml")
    params:
        mafft_params = config["mafft_params"]
    shell:
        """
        cat {input.assemblies} > all.fasta 2> {log}
        mafft {params.mafft_params} --thread {threads} all.fasta > {output.msa} 2>> {log}
        """


rule fasttree:
    input:
        alignment = os.path.join(config['output_directory'], "04-PHYLOGENY", "msa.fasta")
    output:
        tree = os.path.join(config['output_directory'], "04-PHYLOGENY", "tree.nwk")
    log:
        os.path.join(config['output_directory'], "00-Log", "phylogeny", "fasttree.log")
    params:
        extra="",  # Additional arguments
    wrapper:
        "v1.31.1/bio/fasttree"

rule variablility:
    input:
        msa    = os.path.join(config['output_directory'], "04-PHYLOGENY", "msa.fasta"),
        tree = os.path.join(config['output_directory'], "04-PHYLOGENY", "tree.nwk")
    output:
        var    = os.path.join(config['output_directory'], "04-PHYLOGENY", "variability.png"),
        tree   = os.path.join(config['output_directory'], "04-PHYLOGENY", "tree.png")
    log:
        os.path.join(config['output_directory'], "00-Log", "phylogeny", "variability.log")
    threads: 
        1
    conda:
        os.path.join("..", "envs", "variability.yaml")
    params:
        window_size = config["window_size"]
    shell:
        """
        python3 workflow/scripts/variability.py {input.msa} {params.window_size} {output.var} {output.tree} {input.tree} 2> {log}
        """