rule bowtie2_index:
    input:
        config["host_sequence"]
        
    output:
        index = multiext(os.path.join(config['output_directory'], "02-SCREEN", "index", "index"), ".1.bt2", ".2.bt2",".3.bt2",".4.bt2", ".rev.1.bt2", ".rev.2.bt2"),
    params:
        index_dir   = lambda w, output: os.path.split(output[0])[0]
    log:
        os.path.join(config['output_directory'], "00-Log", "bowtie2", "bowtie2_index.log")
    conda:
        os.path.join("..", "envs", "screen.yaml")
    threads:
        16
    shell:
        """
        bowtie2-build --quiet --threads {threads} {input} {params.index_dir}/index 2> {log}
        """


rule bowtie2_map:
    input:
        index = multiext(os.path.join(config['output_directory'], "02-SCREEN", "index", "index"), ".1.bt2", ".2.bt2",".3.bt2",".4.bt2", ".rev.1.bt2", ".rev.2.bt2"),
        r1    = os.path.join(config['output_directory'], "01-QC", "trim", "{sample}.1.fastq"),
        r2    = os.path.join(config['output_directory'], "01-QC", "trim", "{sample}.2.fastq")
    output:
        u1 = os.path.join(config["output_directory"], "02-SCREEN", "bowtie2", "{sample}_decontaminated.1.fastq.gz"),
        u2 = os.path.join(config["output_directory"], "02-SCREEN", "bowtie2", "{sample}_decontaminated.2.fastq.gz")
        
    params:
        index_dir     = lambda w, input: os.path.split(input[0])[0],
        output_pattern = lambda w, output: output[0].replace('.1.', '.%.')
    log:
        os.path.join(config['output_directory'], "00-Log", "bowtie2", "bowtie2_{sample}.log")

    conda:
        os.path.join("..", "envs", "screen.yaml")

    threads:
        4
    shell:
        """
        bowtie2 --quiet --threads {threads} -x {params.index_dir}/index -1 {input.r1} -2 {input.r2} --un-conc-gz {params.output_pattern} 2> {log} > /dev/null
        """

