def get_gff2bed_input(wildcards):
    if config['wgs']:
        return os.path.join(config['output_directory'], "03-ANNOTATION", "prokka", "annotation.gff")
    else:
        return os.path.join(config['annotation'])


rule gff2bed:
    input:
        get_gff2bed_input
    output:
        bed = os.path.join(config['output_directory'], "03-ANNOTATION", "bedtools", "annotation.bed")
    log:
        os.path.join(config['output_directory'], "00-Log", "annotation", "gff2bed.log")
    conda:
        os.path.join("..", "envs", "annotation.yaml")
    threads:
        1
    shell:
        """
        gff2bed < {input} > {output.bed} 2> {log}
        """


checkpoint cut_genes:
    input:
        consensus  = os.path.join(config['output_directory'], "02-ASSEMBLY", "samtools", "{sample}_consensus.fasta"),
        annotation = os.path.join(config['output_directory'], "03-ANNOTATION", "bedtools", "annotation.bed")
    output:
        genes = directory(os.path.join(config['output_directory'], "03-ANNOTATION", "bedtools", "{sample}")),
        genes_all = temp(os.path.join(config['output_directory'], "03-ANNOTATION", "bedtools", "{sample}_genes.fasta"))
    log:
        os.path.join(config['output_directory'], "00-Log", "annotation", "{sample}_cut_genes.log")
    conda:
        os.path.join("..", "envs", "annotation.yaml")
    threads:
        1
    wildcard_constraints:
        sample = f"[^{os.sep}]*"
    shell:
        """
        # we need to make the dir before running awk
        mkdir -p {output.genes} &> {log}
        bedtools getfasta -fi {input.consensus} -bed {input.annotation} -fo {output.genes_all} &>> {log}
        awk '/^>/{{OUT="{output.genes}/"substr($0,2)".fasta"; system("touch "OUT" 2>> {log}"); print ">"substr($0,2)"_sample_{wildcards.sample}" > OUT; next}}{{print > OUT}}' {output.genes_all} 2>> {log}
        """


def get_prokka_input(wildcards):
    if config['polish']=="yes":
        return os.path.join(config['output_directory'], "02-ASSEMBLY", "polish", "assembly_polished.fasta")
    else:
        return os.path.join(config['output_directory'], "02-ASSEMBLY", "spades", "contigs.fasta")


rule prokka:
    input:
        assembly = get_prokka_input
    output:
        outdir = directory(os.path.join(config['output_directory'], "03-ANNOTATION", "prokka")),
        annotation = os.path.join(config['output_directory'], "03-ANNOTATION", "prokka", "annotation.gff")
    log:
        os.path.join(config['output_directory'], "00-Log", "annotation", "prokka.log")
    conda:
        os.path.join("..", "envs", "annotation.yaml")
    threads:
        6
    shell:
        """
        prokka --outdir {output.outdir} --prefix annotation {input.assembly} --force --cpus {threads} &> {log}
        """


def get_annotation(wildcards):
    if config['wgs']:
        return os.path.join(config['output_directory'], "03-ANNOTATION", "prokka", "annotation.gff")
    else:
        return config['annotation']


rule gff2gtf:
    input:
        get_annotation
    output:
        gtf = os.path.join(config['output_directory'], "03-ANNOTATION", "gff2gtf", "annotation.gtf")
    log:
        os.path.join(config['output_directory'], "00-Log", "annotation", "gff2gtf.log")
    conda:
        os.path.join("..", "envs", "annotation.yaml")
    threads:
        1
    shell:
        """
        gffread {input} -T -o {output.gtf}

        # this is supposed to fix compatibility issues of the prokka output with featurecounts
        sed -i 's/CDS/gene/g' {output.gtf}
        sed -i 's/gene_name ".*"//g' {output.gtf}
        """