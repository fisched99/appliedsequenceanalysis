import os

checkpoint check_coverage:
    input:
        genes = expand(os.path.join(config['output_directory'], "03-ANNOTATION", "bedtools", "{sample}"), sample = list(samples.index))
    output:
        gene_list = os.path.join(config['output_directory'], "04-MSA", "gene_list.txt")
    log:
        os.path.join(config['output_directory'], "00-Log", "msa", "check_coverage.log")
    threads: 
        1
    params:
        threshold = config["coverage_threshold"]
    run:
        dict_content = {}
        threshold = float(params.threshold)

        for directory in input:

            for filename in os.listdir(directory):

                if filename.endswith(".fasta"):
                    with open(os.path.join(directory,filename), "r") as file:
                        lines = file.readlines()
                        feature = filename.split('.fasta')[0]

                        if feature in dict_content:
                            if dict_content[feature] ==  0:
                                continue

                        sequence = lines[1]
                        n_count = 0

                        for base in sequence:
                            if base  == 'N':
                                n_count+=1

                        n_content = n_count / len(sequence)

                        # increase count if we want to keep, else set to 0
                        # that way we can tell if a gene was found in all samples with sufficient coverage
                        if n_content <= threshold:
                            if feature in dict_content:
                                dict_content[feature] += 1
                            else:
                                dict_content[feature] = 1
                        else:
                            dict_content[feature] = 0
                    
        with open(output.gene_list, "w") as out:
            for feature, value in dict_content.items():
                # genes that passed all samples will have the same score
                if value == max(dict_content.values()):
                    out.write(feature + '\n')


rule mafft:
    input:
        genes = expand(os.path.join(config['output_directory'], "03-ANNOTATION", "bedtools", "{sample}", "{{gene}}.fasta"), sample = list(samples.index))
    output:
        msa    = os.path.join(config['output_directory'], "07-PHYLOGENY", "mafft", "{gene}_msa.fasta"),
        all    = os.path.join(config['output_directory'], "07-PHYLOGENY", "mafft", "{gene}_all.fasta")
    log:
        os.path.join(config['output_directory'], "00-Log", "phylogeny", "{gene}_mafft.log")
    threads: 
        1
    conda:
        os.path.join("..", "envs", "phylogeny.yaml")
    params:
        mafft_params = config["mafft_params"]
    shell:
        """
        cat {input.genes} > {output.all} 2> {log}
        mafft {params.mafft_params} --thread {threads} {output.all} > {output.msa} 2>> {log}
        """
