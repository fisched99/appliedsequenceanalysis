rule featurecounts:
    input:
        bam = expand(os.path.join(config['output_directory'], "02-ASSEMBLY", "hisat2_map", "{sample}_sorted.bam"), sample = list(samples.index)),
        annotation = os.path.join(config['output_directory'], "03-ANNOTATION", "gff2gtf", "annotation.gtf")
    output:
        counts   = os.path.join(config['output_directory'], "05-COUNTS", "featurecounts", "counts.tsv")
    log:
        os.path.join(config['output_directory'], "00-Log", "counts", "featurecounts.log")
    conda:
        os.path.join("..", "envs", "quantification.yaml")
    threads:
        2
    params:
        featurecounts_params = config['featurecounts']['params']
    shell:
        """
        featureCounts -t transcript -pBP -T {threads} -a {input.annotation} -o {output.counts} {params.featurecounts_params} {input.bam} 2> {log}
        """


rule deseq2:
    input:
        counts_tsv = os.path.join(config['output_directory'], "05-COUNTS", "featurecounts", "counts.tsv"),
        metadata   = config['metadata']
    output:
        dge_results  = os.path.join(config['output_directory'], "06-DGE", "dge_results.csv"),
        volcano_plot = os.path.join(config['output_directory'], "06-DGE","volcano_plot.png"),
        heatmap      = os.path.join(config['output_directory'], "06-DGE","heatmap.png"), 
        ma_plot      = os.path.join(config['output_directory'], "06-DGE","ma_plot.png") 
    log: 
        os.path.join(config['output_directory'], "00-Log", "dge", "dge.log")
    conda:
        os.path.join("..", "envs", "DGE.yaml")
    script:
        os.path.join("..", "scripts", "DGE.R")
        

