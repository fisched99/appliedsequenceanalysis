def checkcheck(wildcards):
    pfff = checkpoints.cut_genes.get(sample = list(samples.index)[0]).output[0]
    with open(checkpoints.check_coverage.get().output[0], 'r') as gene_list_file:
        gene_list = gene_list_file.read().splitlines()
        if not gene_list:
            print("Oops I did it again! The gene list does not contain any gene files to concatenate.\nDeleting gene list to generate new one in next run. Please try again and check input.")
            os.remove(checkpoints.check_coverage.get().output[0])
            exit()
        else:
            return expand(os.path.join(config['output_directory'], "07-PHYLOGENY", "mafft", "{gene}_msa.fasta"), gene = list(gene_list))


rule concat_msa:
    input:
        checkcheck
    output:
        msa_all = os.path.join(config['output_directory'], "07-PHYLOGENY", "msa_all.fasta"),
        tmp = temp(os.path.join(config['output_directory'], "07-PHYLOGENY", "temp"))
    log:
        os.path.join(config['output_directory'], "00-Log", "phylogeny", "concat_msa.log")
    threads: 
        1
    conda:
        os.path.join("..", "envs", "phylogeny.yaml")
    params:
        mafft_params = config["mafft_params"]
    shell:
        """
        # awk magic to remove newline from sequences but not from headers
        gawk -i inplace '{{if(FNR>1){{ORS = sub(/>/, "\\n>") ? "\\n" : "" }} else {{ORS = "\\n"}}}} 1' {input} &> {log}

        # since we can't open 2000 files at the same time on the compute server because of ulimit we have to paste in batches
        touch {output.msa_all} &>> {log}
        count=0
        lst=""
        for f in {input}
            do  
                # note to myself: for ((count++)) '((' returns 1 if expression evaluates to 0,
                # WHICH WILL TERMINATE snakemake because it runs bash in strict mode!!
                # just spend 2h looking for the reason its crashing omfg

                ((++count))
                lst="${{lst}} ${{f}}"

                if [[ count -eq $(($(ulimit -n)-10)) ]]
                    then
                    paste -d'\\0' {output.msa_all} ${{lst}} > {output.tmp} 2>> {log}
                    cp {output.tmp} {output.msa_all} &>> {log}
                    count=0
                    lst=""
                fi
        done

        paste -d'\\0' {output.msa_all} ${{lst}} > {output.tmp} 2>> {log}
        cp {output.tmp} {output.msa_all} &>> {log}

        # reduce sequence headers to only one displaying the samplename
        sed -i 's/.*sample_/>/'  {output.msa_all} &>> {log}
        """


rule fasttree:
    input:
        alignment = os.path.join(config['output_directory'], "07-PHYLOGENY", "msa_all.fasta")
    output:
        tree = os.path.join(config['output_directory'], "07-PHYLOGENY", "tree.nwk")
    log:
        os.path.join(config['output_directory'], "00-Log", "phylogeny", "fasttree.log")
    params:
        extra="-fastest"  # Additional arguments
    wrapper:
        "v1.31.1/bio/fasttree"


rule draw_tree:
    input:
        tree = os.path.join(config['output_directory'], "07-PHYLOGENY", "tree.nwk")
    output:
        tree = os.path.join(config['output_directory'], "07-PHYLOGENY", "tree.png")
    log:
        os.path.join(config['output_directory'], "00-Log", "phylogeny", "draw_tree.log")
    conda:
        os.path.join("..", "envs", "draw_tree.yaml")
    script:
        os.path.join("..", "scripts", "draw_tree.py")