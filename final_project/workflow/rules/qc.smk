rule fastp_rna:
    input:
        sample = lambda wildcards: [ samples.at[wildcards.sample, 'fq1'], samples.at[wildcards.sample, 'fq2']]
    output:
        trimmed=[os.path.join(config['output_directory'], "01-QC", "trim_rna", "{sample}.1.fastq"),
                 os.path.join(config['output_directory'], "01-QC", "trim_rna", "{sample}.2.fastq")],

        # Unpaired reads separately
        unpaired1=os.path.join(config['output_directory'], "01-QC", "trim_rna", "{sample}.u1.fastq"),
        unpaired2=os.path.join(config['output_directory'], "01-QC", "trim_rna", "{sample}.u2.fastq"),

        #merged="trimmed/pe/{sample}.merged.fastq",
        failed=os.path.join(config['output_directory'], "01-QC", "trim_rna", "{sample}.failed.fastq"),
        html=os.path.join(config['output_directory'], "01-QC", "qc_rna", "{sample}_fastp.html"),
        json=os.path.join(config['output_directory'], "01-QC", "qc_rna", "{sample}_fastp.json")
    log:
        os.path.join(config['output_directory'], "00-Log", "qc_rna", "{sample}_fastp.log")
    params:
        #adapters="--adapter_sequence ACGGCTAGCTA --adapter_sequence_r2 AGATCGGAAGAGCACACGTCTGAACTCCAGTCAC"
        #extra="--merge"
    threads: 2
    wrapper:
        "v1.31.1/bio/fastp"


rule fastp_wgs:
    input:
        sample = lambda wildcards: [ wgs.at[wildcards.sample, 'fq1'], wgs.at[wildcards.sample, 'fq2']]
    output:
        trimmed=[os.path.join(config['output_directory'], "01-QC", "trim_wgs", "{sample}.1.fastq"),
                 os.path.join(config['output_directory'], "01-QC", "trim_wgs", "{sample}.2.fastq")],

        # Unpaired reads separately
        unpaired1=os.path.join(config['output_directory'], "01-QC", "trim_wgs", "{sample}.u1.fastq"),
        unpaired2=os.path.join(config['output_directory'], "01-QC", "trim_wgs", "{sample}.u2.fastq"),

        #merged="trimmed/pe/{sample}.merged.fastq",
        failed=os.path.join(config['output_directory'], "01-QC", "trim_wgs", "{sample}.failed.fastq"),
        html=os.path.join(config['output_directory'], "01-QC", "qc_wgs", "{sample}_fastp.html"),
        json=os.path.join(config['output_directory'], "01-QC", "qc_wgs", "{sample}_fastp.json")
    log:
        os.path.join(config['output_directory'], "00-Log", "qc_wgs", "{sample}_fastp.log")
    params:
        #adapters="--adapter_sequence ACGGCTAGCTA --adapter_sequence_r2 AGATCGGAAGAGCACACGTCTGAACTCCAGTCAC"
        #extra="--merge"
    threads: 2
    wrapper:
        "v1.31.1/bio/fastp"


def multiqc_input(wildcards):
    if config['wgs']:
        return expand(os.path.join(config['output_directory'], "01-QC", "qc_wgs", "{sample}_fastp.json"), sample = (list(wgs.index) + list(samples.index)))
    else:
        return expand(os.path.join(config['output_directory'], "01-QC", "qc_rna", "{sample}_fastp.json"), sample = list(samples.index))


rule multiqc:
    input:
        multiqc_input
    output:
        os.path.join(config['output_directory'], "01-QC", "multiqc.html")
    params:
        extra="",  # Optional: extra parameters for multiqc.
        use_input_files_only=True
    log:
        os.path.join(config['output_directory'], "00-Log", "multiqc", "mutliqc_all.log")
    wrapper:
        "v1.31.1/bio/multiqc"