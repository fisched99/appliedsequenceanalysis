def get_hisat_ref(wildcards):
    if config['wgs']:
        if config['polish']=="yes":
            return os.path.join(config['output_directory'], "02-ASSEMBLY", "polish", "assembly_polished.fasta")
        else:
            return os.path.join(config['output_directory'], "02-ASSEMBLY", "spades", "contigs.fasta")
    else:
        return config['reference']


rule hisat2_idx:
    input:
        reference = get_hisat_ref
    output:
        index = multiext(os.path.join(config['output_directory'], "02-ASSEMBLY", "hisat2-index", f"{os.path.splitext(os.path.basename(config['reference']))[0] if config['reference'] else 'assembly'}"), '.1.ht2', '.2.ht2', '.3.ht2', '.4.ht2', '.5.ht2', '.6.ht2', '.7.ht2', '.8.ht2')
    log:
        os.path.join(config['output_directory'], "00-Log", "assembly", "hisat2_idx.log")
    conda:
        os.path.join("..", "envs", "mapping.yaml")
    threads:
        16
    params:
        idx_prefix = lambda w, output: os.path.splitext(os.path.splitext(output[0])[0])[0]
    shell:
        """
        hisat2-build -p {threads} {input.reference} {params.idx_prefix} &> {log}
        """


rule hisat2_map:
    input:
        r1 = os.path.join(config['output_directory'], "01-QC", "trim_rna", "{sample}.1.fastq"),
        r2 = os.path.join(config['output_directory'], "01-QC", "trim_rna", "{sample}.2.fastq"),
        index = multiext(os.path.join(config['output_directory'], "02-ASSEMBLY", "hisat2-index", f"{os.path.splitext(os.path.basename(config['reference']))[0] if config['reference'] else 'assembly'}"), '.1.ht2', '.2.ht2', '.3.ht2', '.4.ht2', '.5.ht2', '.6.ht2', '.7.ht2', '.8.ht2')
    output:
        sorted_bam = os.path.join(config['output_directory'], "02-ASSEMBLY", "hisat2_map", "{sample}_sorted.bam")
    log:
        os.path.join(config['output_directory'], "00-Log", "assembly", "{sample}_hisat2_map.log")
    conda:
        os.path.join("..", "envs", "mapping.yaml")
    threads:
        6
    params:
        idx_path = os.path.join(config['output_directory'], "02-ASSEMBLY", "hisat2-index"),
        idx_prefix = os.path.splitext(os.path.basename(config['reference']))[0]
    shell:
        """
        export HISAT2_INDEXES='{params.idx_path}'
        hisat2 -x {params.idx_prefix} -1 {input.r1} -2 {input.r2} -p {threads} 2> {log} | samtools view -bS 2> {log} | samtools sort -o {output} -T tmp --threads {threads} 2> {log}
        """


rule samtools_consensus:
    input:
        sorted_bams = os.path.join(config['output_directory'], "02-ASSEMBLY", "hisat2_map", "{sample}_sorted.bam")
    output:
        consensus = os.path.join(config['output_directory'], "02-ASSEMBLY", "samtools", "{sample}_consensus.fasta")
    log:
        os.path.join(config['output_directory'], "00-Log", "assembly", "{sample}_samtools.log")
    conda:
        os.path.join("..", "envs", "mapping.yaml")
    threads:
        1
    shell:
        """
        samtools consensus -f fasta -o {output.consensus} -d 5 {input.sorted_bams} 2> {log}
        """


def get_spades_input(wildcards):
    if config["host_sequence"]:
        return { "r1" : expand(os.path.join(config["output_directory"], "01-QC", "screen", "samtools", "{sample}_decontaminated.1.fastq"), sample = list(wgs.index)),
                 "r2" : expand(os.path.join(config["output_directory"], "01-QC", "screen", "samtools", "{sample}_decontaminated.2.fastq"), sample = list(wgs.index)) }
    else:
        return {"r1" : expand(os.path.join(config['output_directory'], "01-QC", "trim_wgs", "{sample}.1.fastq"), sample = list(wgs.index)),
                "r2" : expand(os.path.join(config['output_directory'], "01-QC", "trim_wgs", "{sample}.2.fastq"), sample = list(wgs.index)) }


rule spades:
    input:
        unpack(get_spades_input)
    output:
        contigs   = os.path.join(config['output_directory'], "02-ASSEMBLY", "spades", "contigs.fasta"),
        scaffolds = os.path.join(config['output_directory'], "02-ASSEMBLY", "spades", "scaffolds.fasta"),
        reads_1   = os.path.join(config['output_directory'], "02-ASSEMBLY", "spades", "reads_1.fastq"),
        reads_2   = os.path.join(config['output_directory'], "02-ASSEMBLY", "spades", "reads_2.fastq"),
        outdir    = directory(os.path.join(config['output_directory'], "02-ASSEMBLY", "spades"))
    log:
        os.path.join(config['output_directory'], "00-Log", "assembly", "spades.log")
    conda:
        os.path.join("..", "envs", "assembly.yaml")
    threads:
        16
    shell:
        """
        cat {input.r1} > {output.reads_1}
        cat {input.r2} > {output.reads_2}
        spades.py -o {output.outdir} -1 {output.reads_1} -2 {output.reads_2} -t {threads} --phred-offset 33 > {log}
        """


rule polishing:
    input:
        assembly   = os.path.join(config['output_directory'], "02-ASSEMBLY", "spades", "contigs.fasta"),
        reads_1   = os.path.join(config['output_directory'], "02-ASSEMBLY", "spades", "reads_1.fastq"),
        reads_2   = os.path.join(config['output_directory'], "02-ASSEMBLY", "spades", "reads_2.fastq")
    output:
        polished     = os.path.join(config['output_directory'], "02-ASSEMBLY", "polish", "assembly_polished.fasta")
    log:
        os.path.join(config['output_directory'], "00-Log", "assembly", "polca.log")
    threads: 
        2
    conda:
        os.path.join("..", "envs", "assembly.yaml")
    shadow: # we need to do this becauzse polca saves output files to the project directory by default and overwrites the other instances output and crashes because its dumb
        "minimal"
    shell:
        """
        polca.sh -a {input.assembly} -r '{input.reads_1} {input.reads_2}' -t {threads} &> {log}

        # move output file to output because polca is stupid and doesnt work
        mv *.PolcaCorrected.fa {output.polished} 2>> {log}
        """