#### redirect output to log file
zz <- file(snakemake@log[[1]], open = "wt")
sink(zz, type = "message")
sink(zz, type = "output")

### load libraries
library(DESeq2)
library(ggplot2)
library(pheatmap)
library(EnhancedVolcano)


colorblind_friendly_pal <- c("#000000", "#E69F00", "#56B4E9", "#009E73", "#F0E442", "#0072B2", "#D55E00", "#CC79A7")

### read in feature counts output and with first line as header and column 'geneid' as rownames
counts <- read.table(snakemake@input[[1]], header=TRUE, row.names = 1)
condition <- read.table(snakemake@input[[2]], header=TRUE, row.names = 1)

colnames(condition)[1] <- 'group'

### remove first 6 columns since they contain gene information but we only want count data now and convert to numeric
counts <- counts[,6:ncol(counts)]
counts[] <- sapply(counts, as.numeric)

### clean samplenames in count matrix
colnames(counts) <- gsub('.*_map.', '', colnames(counts))
colnames(counts) <- gsub('_R_sorted.bam', '', colnames(counts))

### check if all samples from condition sheet are present in count matrix too
if (! all(rownames(condition) %in% colnames(counts)) ) {
  print('Warning: samplenames declared in metadata sheet could not be found in count matrix because they are missing or are missspelled, please check these files.')
}

### check if samplenames are equal in both sets
if (! all(rownames(condition) == colnames(counts)) ) {
  print('Warning: samplenames declared in count matrix do not equal samplenames declared in metadata sheet, please check these files.')
}

### check if there is same number of samples in count matrix and metadata
if (! all(rownames(condition) == colnames(counts)) ) {
  print('Warning: unequal amount of samples declared in metadata and count matrix. Omitting excess samples, this might falsify results. Please make sure both files declare the same samples.')
  counts <- counts[ , 1:min(ncol(counts), nrow(condition))]
  condition <- head(condition, n = min(ncol(counts), nrow(condition)))
}


dds <- DESeqDataSetFromMatrix(countData = counts,
                                      colData = condition,
                                      design = ~ group)
        
dds <- DESeq(dds)
res <- results(dds)
sorted_res <- res[order(res$padj, -res$log2FoldChange),] # sort rows for padj and then decreasing logfoldchange
sorted_res <- sorted_res[1:20,!names(res) %in% c("stat", "baseMean", "pvalue","lfcSE")] # only take the first 20 rows and some columns

print(paste('Writing results to output file as', as.character(snakemake@output[['dge_results']]), '..'))
write.csv(as.data.frame(sorted_res), snakemake@output[['dge_results']], row.names=TRUE)


print(paste('Creating volcano plot as ', snakemake@output[['volcano_plot']], '..'))
volcano_data <- data.frame(Log2FoldChange = res$log2FoldChange, Log10AdjustedPvalue = -log10(res$padj))


plot.volcano <-function(df, title_txt, subtitle_txt){
  EnhancedVolcano(df, x = "log2FoldChange", y = "padj", ylab = expression(-Log[10]~(p-value)),
                  title = title_txt, subtitle = paste("Volcano plot:", subtitle_txt, ", \u03b1 = ", 0.05, sep = " "),
                  pCutoff = 0.05, FCcutoff = 1, lab = rownames(df),
                  colAlpha = 1, boxedLabels = TRUE,
                  drawConnectors = TRUE,
                  widthConnectors = 1.0,
                  colConnectors = 'black',
                  max.overlaps = 30,
                  legendLabels = c('not significant', expression(Log[2]~FC~sig), expression(adj.~p-value~sig), expression(adj.~p-value~and~log[2]~FC~sig)))
}


volcano_plot <- plot.volcano(res, "", "subtitle")
bin <- ggsave(plot=volcano_plot, filename=snakemake@output[['volcano_plot']], width=8, height=6, dpi=300)


#Heatmap
print(paste('Creating clustered heatmap as', snakemake@output[['heatmap']], '..'))
de_genes <- rownames(subset(res, padj < 0.05))
de_counts <- counts(dds)[de_genes, rownames(condition)]
log2_de_counts <- log2(de_counts + 1)

heatmap <- pheatmap(log2_de_counts, 
         scale="row", 
         cellheight=6,
         clustering_distance_cols="euclidean",
         clustering_distance_rows="euclidean",
         clustering_method="complete",
         main="Heatmap of Differentially Expressed Genes",
         fontsize=8)

bin <- ggsave(plot=heatmap, filename=snakemake@output[['heatmap']], width=6, height=4, dpi=300)


#MA plot
print(paste('Creating minus average plot as', snakemake@output[['ma_plot']], " .. "))

plot.ma <- function(dds_res, title_txt, alpha){
  df <- data.frame(baseMean = dds_res$baseMean, log2FoldChange = dds_res$log2FoldChange, padj = dds_res$padj)
  df$sig <- ifelse(df$padj < alpha, "1", "0")
  ggplot() +
    geom_point(data = df, aes(x = baseMean, y = log2FoldChange, col = sig), size = 0.5, show.legend = FALSE) +
    ggtitle(paste("MA-Plot:", title_txt, ", \u03b1 = ", alpha, sep = " ")) +
    theme(plot.title = element_text(size = 11)) +
    theme(plot.title = element_text(hjust = 0.5)) +
    scale_colour_manual(values = colorblind_friendly_pal) +
    ylab("log2 fold change") +
    xlab("mean of normalized counts") 
}

plot_ma <- plot.ma(res, 'differentially expressed features, significatn features marked yellow', 0.05)
bin <- ggsave(plot=plot_ma, snakemake@output[['ma_plot']], width=7, height=4, dpi=300)

### close sink output redirection
sink(type = "message")
sink(type = "output")



