import matplotlib.pyplot as plt
from Bio import Phylo
import sys


stdout = sys.stdout
stderr = sys.stderr

with open(snakemake.log[0], 'w') as sys.stdout:

    sys.stderr = sys.stdout
    Phylo.draw(Phylo.read(snakemake.input[0], "newick"), do_show = False)
    plt.title(f"Phylogenetic tree over all samples clustered by variance of genes")
    plt.savefig(snakemake.output[0])

sys.stdout = stdout
sys.stderr = stderr