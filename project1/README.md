# AppliedSequenceAnalysis
## Group 2
Project code for the masters course applied sequence analysis.

## Run pipeline
To run the pipeline define either the `samples.tsv` containing the samplenames and paths to the different sequence files, or the directory containing the sequence files, either via the config file (`project1/config/config.yaml`) or as a command line argument using the respective flags `input_directory` and `samples`. You then also have to provide a reference genome file for the mapping rule:

```
snakemake --profile config/local --config samples=input.tsv ref=path/to/ref.fa
```
or
```
snakemake --profile config/local --config input_directory=path/to/sequence/files ref=path/to/ref.fa
```