
import os
from collections import defaultdict

def absoluteFilePaths(directory):
    for dirpath,_,filenames in os.walk(directory):
        for f in filenames:
            yield os.path.abspath(os.path.join(dirpath, f))


fastq_dir   = config["input_directory"]
mode        = "paired"
dictionary = defaultdict(list)

files = sorted(list(absoluteFilePaths(fastq_dir)))

with open('input.tsv', 'w') as f:
    print("sample\tfq1\tfq2", file=f)

    for i in range(len(files)-1):
        r1 = files[i]
        r2 = files[i+1]
        prefix = os.path.commonprefix([r1, r2]).split(os.path.sep)[-1]

        if i % 2 == 0:
            print(f"{prefix.strip()}\t{r1.strip()}\t{r2}", file=f)
        


print(f"{bcolors.OKBLUE}INFO: Writing input.tsv.")



