
rule fastqc_pre:
    input:
        os.path.join(INPUT_DIR, "{sample}{R}.fastq.gz")
    output:
        hmtl   = os.path.join(config["output_directory"], "01-QC", "qc_pre", "{sample}{R}_fastqc.html"),
        zip    = os.path.join(config["output_directory"], "01-QC", "qc_pre", "{sample}{R}_fastqc.zip")
    log:
        os.path.join(config['output_directory'], "00-Log", "qc_pre", "{sample}{R}_fastqc_pre.log")
    conda:
        os.path.join("..", "envs", "qc.yaml")
    threads:
        4
    params:
        outdir = os.path.join(config["output_directory"], "01-QC", "qc_pre")
    shell:
        """
        fastqc --quiet --threads {threads} {input} -o {params.outdir} 2> {log}
        """


rule trimmomatic_pe:
    input:
        r1    = lambda wildcards: samples.at[wildcards.sample, 'fq1'],
        r2    = lambda wildcards: samples.at[wildcards.sample, 'fq2']
    output:
        r1 = os.path.join(config['output_directory'], "01-QC", "trim", "{sample}.1.fastq.gz"),
        r2 = os.path.join(config['output_directory'], "01-QC", "trim", "{sample}.2.fastq.gz"),
        # reads where trimming entirely removed the mate
        r1_unpaired = os.path.join(config['output_directory'], "01-QC", "trim", "{sample}.1.unpaired.fastq.gz"),
        r2_unpaired = os.path.join(config['output_directory'], "01-QC", "trim", "{sample}.2.unpaired.fastq.gz")
    log:
        os.path.join(config['output_directory'], "00-Log", "trim", "{sample}_trimmomatic.log")
    params:
        # list of trimmers (see manual)
        trimmer=["TRAILING:3"],
        # optional parameters
        extra="",
        compression_level="-9"
    threads:
        4
    # optional specification of memory usage of the JVM that snakemake will respect with global
    # resource restrictions (https://snakemake.readthedocs.io/en/latest/snakefiles/rules.html#resources)
    # and which can be used to request RAM during cluster job submission as `{resources.mem_mb}`:
    # https://snakemake.readthedocs.io/en/latest/executing/cluster.html#job-properties
    resources:
        mem_mb=1024
    wrapper:
        "v1.28.0/bio/trimmomatic/pe"

rule fastqc_post:
    input:
        os.path.join(config['output_directory'], "01-QC", "trim", "{sample}{R}.fastq.gz")
    output:
        hmtl   = os.path.join(config["output_directory"], "01-QC", "qc_post", "{sample}{R}_fastqc.html"),
        zip    = os.path.join(config["output_directory"], "01-QC", "qc_post", "{sample}{R}_fastqc.zip")
    log:
        os.path.join(config['output_directory'], "00-Log", "qc_post", "{sample}{R}_fastqc_post.log")
    conda:
        os.path.join("..", "envs", "qc.yaml")
    threads:
        4
    params:
        outdir = os.path.join(config["output_directory"], "01-QC", "qc_post")
    shell:
        """
        fastqc --quiet --threads {threads} {input} -o {params.outdir} 2> {log}
        """

rule qualimap:
    input:
        os.path.join(config['output_directory'], "03-SORT", "{sample}_sorted.bam")
    output:
        directory(os.path.join(config["output_directory"], "01-QC", "qualimap", "{sample}"))
    log:
        os.path.join(config['output_directory'], "00-Log", "qualimap", "{sample}_qualimap.log")
    conda:
        os.path.join("..", "envs", "qc.yaml")
    threads:
        1
    shell:
        """
        qualimap bamqc -bam {input} -outdir {output} > {log}
        """

rule multiqc:   
    input:
        fasqtcpre  = expand(os.path.join(config["output_directory"], "01-QC", "qc_pre", "{sample}{R}_fastqc.zip"), sample = list(samples.index), R = [1, 2]),
        fasqtcpost = expand(os.path.join(config["output_directory"], "01-QC", "qc_post", "{sample}.{R}_fastqc.zip"), sample = list(samples.index), R = [1, 2]),
        qualimap = expand(os.path.join(config["output_directory"], "01-QC", "qualimap", "{sample}"), sample = list(samples.index))
    output:
        directory(os.path.join(config["output_directory"], "01-QC", "multiqc"))
    params:
        ""  # Optional: extra parameters for multiqc.
    log:
        os.path.join(config['output_directory'], "00-Log", "multiqc", "multiqc.log")
    conda:
        os.path.join("..", "envs", "qc.yaml")
    shell:
        """
        multiqc --quiet {input[0]} {input[1]} {input[2]} --outdir {output}  2> {log}
        """

