rule sam_to_bam:
    input: 
        os.path.join(config['output_directory'], "01-BOWTIE2", "{sample}.sam")

    output: 
        os.path.join(config['output_directory'], "02-BAM", "{sample}.bam")
    
    log:
        os.path.join(config['output_directory'], "00-Log", "sam_to_bam", "{sample}.log")

    conda:
        os.path.join("..", "envs", "samtools.yaml")
    
    threads:
        4

    shell:
        "samtools view -b -@ {threads} {input} > {output} 2> {log}"
        
rule sort_bam:
    input:
        os.path.join(config['output_directory'], "02-BAM", "{sample}.bam")
    
    output:
        os.path.join(config['output_directory'], "03-SORT", "{sample}_sorted.bam")

    log:
        os.path.join(config['output_directory'], "00-Log", "sort_bam", "{sample}.log")
    conda:
        os.path.join("..", "envs", "samtools.yaml")

    threads:
        4
    
    shell:
        "samtools sort -@ {threads} {input} > {output} 2> {log}"
        
rule index_bam:
    input: 
        os.path.join(config['output_directory'], "03-SORT", "{sample}_sorted.bam")

    output: 
        os.path.join(config['output_directory'], "04-IDXBAM", "{sample}_sorted.bam.bai")
    
    log:
        os.path.join(config['output_directory'], "00-Log", "index_bam", "{sample}.log")

    conda:
        os.path.join("..", "envs", "samtools.yaml")
    
    threads:
        4
    params:
        sorted_bam_directory = os.path.join(config['output_directory'], "03-SORT")

    shell:
        """
            samtools index -b -@ {threads} {input} {output} 2> {log}
            cp {output} {params.sorted_bam_directory}/
        """

rule idx_stats:
    input: 
        index_bam  = os.path.join(config['output_directory'], "04-IDXBAM", "{sample}_sorted.bam.bai"),
        sorted_bam = os.path.join(config['output_directory'], "03-SORT", "{sample}_sorted.bam")

    output: 
        os.path.join(config['output_directory'], "05-IDXSTATS", "raw", "{sample}_stats.txt")
    
    log:
        os.path.join(config['output_directory'], "00-Log", "idx_stats", "{sample}.log")

    conda:
        os.path.join("..", "envs", "samtools.yaml")
    
    threads:
        1

    shell:
        "samtools idxstats {input.sorted_bam} > {output} 2> {log}"

rule augment_idx_stats:
    input: 
        os.path.join(config['output_directory'], "05-IDXSTATS", "raw", "{sample}_stats.txt")

    output: 
        os.path.join(config['output_directory'], "05-IDXSTATS", "augmented", "{sample}_stats_aug.txt")
    
    log:
        os.path.join(config['output_directory'], "00-Log", "augment_idx_stats", "{sample}.log")
    
    threads:
        1
    run:
        stats_df = pd.read_csv(input[0], sep='\t')
        stats_df.columns = ['ref_name', 'seq_len', 'mapped_reads', 'unmapped_reads']
        stats_df["coverage"] = (stats_df["mapped_reads"] + stats_df["unmapped_reads"]) / (stats_df["seq_len"] / 1000)
        stats_df.to_csv(output[0], sep = "\t")
