rule bowtie2_index:
    input:
        config["ref"]
        
    output:
        index = multiext(os.path.join(config['output_directory'], "01-BOWTIE2", "index", "index"), ".1.bt2", ".2.bt2",".3.bt2",".4.bt2", ".rev.1.bt2", ".rev.2.bt2"),
    params:
        index_dir   = lambda w, output: os.path.split(output[0])[0],
        quiet       = config["bowtie2"]["quiet"]

    log:
        os.path.join(config['output_directory'], "00-Log", "bowtie2", "bowtie2_index.log")
    conda:
        os.path.join("..", "envs", "bowtie2.yaml")
    threads:
        4
    shell:
        """
        bowtie2-build {params.quiet} --threads {threads} {input} {params.index_dir}/index 2> {log}
        export BOWTIE2_INDEXES={params.index_dir} 2>> {log}
        """


rule bowtie2_map:
    input:
        index = multiext(os.path.join(config['output_directory'], "01-BOWTIE2", "index", "index"), ".1.bt2", ".2.bt2",".3.bt2",".4.bt2", ".rev.1.bt2", ".rev.2.bt2"),
        r1    = lambda wildcards: samples.at[wildcards.sample, 'fq1'],
        r2    = lambda wildcards: samples.at[wildcards.sample, 'fq2']
    output:
        os.path.join(config["output_directory"], "01-BOWTIE2", "{sample}.sam")
        
    params:
        index_dir     = lambda w, input: os.path.split(input[0])[0],
        file_format = config["bowtie2"]["format"],
        quiet = config["bowtie2"]["quiet"]
    log:
        os.path.join(config['output_directory'], "00-Log", "bowtie2", "bowtie2_{sample}.log")

    conda:
        os.path.join("..", "envs", "bowtie2.yaml")

    threads:
        4
    shell:
        """
        bowtie2 {params.quiet} --threads {threads} {params.file_format} -x {params.index_dir}/index -1 {input.r1} -2 {input.r2} -S {output} 2> {log} > /dev/null
        """
